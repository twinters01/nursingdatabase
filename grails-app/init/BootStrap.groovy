
import nursingdatabase.Student

class BootStrap {

    def init = { servletContext ->
        def s = new nursingdatabase.Student(name: "Tyler", email: "twinters01@hotmail.com", yearsEDNursing: 10, yearsNursing: 10, levelOfEducation: "BS")
        s.save()
        
        s = new nursingdatabase.Student(name: "asd", email: "twiwters01@hotmail.com", yearsEDNursing: 10, yearsNursing: 10, levelOfEducation: "BS")
        s.save()
    }
    def destroy = {
    }
}
