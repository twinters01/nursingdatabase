<%@ page contentType="text/html;charset=UTF-8" %>
<html>
    <head>
        <title>Welcome to Student Registration</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
        <script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
        <link rel="stylesheet" type="text/css" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">

    </head>
    <body>
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-2"></div>
                <div class="col-md-8">
                    
                    <div class="row">
                        <div class="panel panel-default">
                            <div class="panel-heading">Emergency Nursing Database</div>
                            <div class="panel-body">
                                <div class="col-md-4"></div>
                                <div class="col-md-4">
                                    <g:if test="${flash.error}">
                                        <div style="display: block" class="alert alert-danger">${flash.error}</div>
                                    </g:if>
                                    <g:if test="${flash.success}">
                                        <div class="alert alert-success">${flash.success}</div>
                                    </g:if>
                                    <!-- Input form -->
                                    <g:form controller='Student' action='register'>
                                        <label for="name">Name:</label>
                                        <div class="input-group">
                                            <g:textField class="form-control" name="name"/>
                                        </div>

                                        <label for="email">Email:</label>
                                        <div class="input-group">
                                            <g:textField class="form-control ${hasErrors(bean:student,field:'email','errors')}" name="email"/>
                                        </div>

                                        <label for="EdNursing">Years of ED nursing:</label>
                                        <div class="input-group">
                                            <g:textField class="form-control" name="yearsEDNursing"/>
                                        </div>

                                        <label for="Nursing">Years of nursing:</label>
                                        <div class="input-group">
                                            <g:textField class="form-control" name="yearsNursing"/>
                                        </div>

                                        <label for="levelOfEducation">Level of education</label>
                                        <select class="form-control" name="levelOfEducation">
                                            <option value="Diploma">Diploma</option>
                                            <option value="AS">Associates</option>
                                            <option value="BS">Bachelors</option>
                                            <option value="MS">Masters</option>
                                            <option value="Doctoral">Doctorate</option>
                                        </select>
                                        <br>
                                            
                                        <g:submitButton class='btn' name='register' value='Submit'/>  
                                    </g:form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>