package nursingdatabase

class Student 
{
    String name
    String email
    Integer yearsEDNursing
    Integer yearsNursing
    String levelOfEducation
    Date dateEntered = new Date()
    
    static constraints = 
    {
        name blank: false
        email blank: false, unique: true, size: 5..15, matches: "[a-zA-Z0-9]+@[a-zA-Z0-9]+.[a-zA-Z0-9]+"
        yearsEDNursing blank: false
        yearsNursing blank:false
        levelOfEducation inList: ["Diploma", "AS", "BS", "MS", "Doctoral"]
    }
}
