package nursingdatabase
//No longer needed
class Administrator 
{
    String name
    String username
    String password
    
    static constraints = 
    {
        name blank: false
        username blank: false, unique: true
        password blank: false, password: true, size: 5..15
    }
}
