package nursingdatabase

class StudentController {
    
    //Lists all students
    def list = {
        def pass="test"
        if(!params.password && !session.admin)
        {
            redirect(action:'login')
        }
        else if(pass.equals(params.password) || session.admin)       
        {
            session.admin = true
            def students = Student.list()
            return [students: students]
        }
        else
        {
            flash.error = "Incorrect password"
            redirect(action:'login')
        }
        return
    }
    
    def login=
    {
    }
    
    def update={
        def s = Student.get(params.id)
        s.name = params.name
        s.email = params.email
        s.yearsEDNursing = params.yearsEDNursing.toInteger()
        s.yearsNursing = params.yearsNursing.toInteger()
        if(params.levelOfEducation != 'UC')
        {
            s.levelOfEducation = params.levelOfEducation
        }
        if(s.save())
        {
            flash.success = s.name + " entry modified"
            redirect(action:"list")
        }
        else
        {
            flash.error = ""
            if(s.errors.hasFieldErrors("name"))
            {
                flash.error += "Please enter a valid name. "
            }
            if(s.errors.hasFieldErrors("email"))
            {
                flash.error += "Please enter a valid email. "
            }
            if(s.errors.hasFieldErrors("yearsEDNursing"))
            {
                flash.error += "Please enter a valid number for years of ED nursing. "
            }
            if(s.errors.hasFieldErrors("yearsNursing"))
            {
                flash.error += "Please enter a valid number for years of nursing. "
            }
            redirect(action:'list')
        }
    }
    
    def delete ={
        def student = Student.get(params.id)
        try{
            student.delete()
            flash.success = "Entry deleted!"
            redirect(action:'list')
        }
        catch(Exception e)
        {
            flash.error = "Entry not deleted, exception: " + e + "\n Student: " + params.id
            redirect(action:'list')
        }
    }
    
    def register = {
        def s = new Student(params)
        flash.message=params
        if(!s.validate())
        {
            flash.error = ""
            if(s.errors.hasFieldErrors("name"))
            {
                flash.error += "Please enter a valid name. "
            }
            if(s.errors.hasFieldErrors("email"))
            {
                flash.error += "Please enter a valid email. "
            }
            if(s.errors.hasFieldErrors("yearsEDNursing"))
            {
                flash.error += "Please enter a valid number for years of ED nursing. "
            }
            if(s.errors.hasFieldErrors("yearsNursing"))
            {
                flash.error += "Please enter a valid number for years of nursing. "
            }
            
            redirect(uri:'/')
        }
        else
        {
            s.save()
            flash.success = s.name + " successfully registered to database!"
            redirect(uri:'/')
        }
        
        return
    }
   
}
