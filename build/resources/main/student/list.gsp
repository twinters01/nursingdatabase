<!--
  To change this license header, choose License Headers in Project Properties.
  To change this template file, choose Tools | Templates
  and open the template in the editor.
-->

<%@ page contentType="text/html;charset=UTF-8" %>

<html>
    
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        
        <title>Students</title>
    </head>
    <body>
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-2"> </div>
                <div class="col-md-8">
                    <div class="row">
                    <g:if test="${flash.error}">
                        <div class="alert alert-danger">${flash.error}</div>
                    </g:if>
                    <g:if test="${flash.success}">
                        <div class="alert alert-success">${flash.success}</div>
                    </g:if>    
                    <div class="panel panel-default">
                        <div class="panel-heading">Students</div>
                            <table class="table">
                                <tr>
                                    <td>Name</td>
                                    <td>Email</td>
                                    <td>Years of ED Nursing</td>
                                    <td>Years of Nursing</td>
                                    <td>Level of Education</td>
                                </tr>
                                <g:each in="${students}" var="student">
                                    <g:form>
                                    <tr>
                                        <td name="name">${student.name}</td>
                                        <td name="email">${student.email}</td>
                                        <td name="yearsEDNursing">${student.yearsEDNursing}</td>
                                        <td name="yearsNursing">${student.yearsNursing}</td>
                                        <td name="levelOfEducation">${student.levelOfEducation}</td>
                                        <td>
                                            <div class="btn-group">
                                                <g:hiddenField name="id" value='${student.id}'/>
                                                
                                                <!-- Edit button -->
                                                <button class="btn btn-warning" type='button' data-toggle='modal' data-target='#editModal'>
                                                    Edit
                                                </button>
                                                <!-- Delete button -->
                                                <!--<button type="button" class="btn btn-danger" data-toggle="modal" data-target="#deleteModal">-->
                                                <g:actionSubmit type="button" class="btn btn-danger" action="delete" onclick="return confirm('Are you sure?');" value="Delete">
                                                </g:actionSubmit>

                                                <!-- Editing Modal -->
                                                <div id="editModal" class="modal fade">
                                                    <div class="modal-dialog">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span></button>
                                                                <h1 class="modal-title">Edit Entry</h1>
                                                            </div>
                                                            
                                                            <div class='modal-body'>
                                                                <!-- TODO edit dialog -->
                                                                <g:form controller="Student" action="update">
                                                                    <g:hiddenField name="id" value="${student.id}"/>
                                                                    
                                                                    <label for="name">Name:</label>
                                                                    <div class="input-group">
                                                                        <g:textField class="form-control" name="name" value="${student.name}"/>
                                                                    </div>

                                                                    <label for="email">Email:</label>
                                                                    <div class="input-group">
                                                                        <g:textField class="form-control" name="email" value="${student.email}"/>
                                                                    </div>

                                                                    <label for="EdNursing">Years of ED nursing:</label>
                                                                    <div class="input-group">
                                                                        <g:textField class="form-control" name="yearsEDNursing" value="${student.yearsEDNursing}"/>
                                                                    </div>

                                                                    <label for="Nursing">Years of nursing:</label>
                                                                    <div class="input-group">
                                                                        <g:textField class="form-control" name="yearsNursing" value="${student.yearsNursing}"/>
                                                                    </div>

                                                                    <label for="levelOfEducation">Level of education</label>
                                                                    <select class="form-control" name="levelOfEducation">
                                                                        <option value="UC">Unchanged</option>
                                                                        <option value="Diploma">Diploma</option>
                                                                        <option value="AS">Associates</option>
                                                                        <option value="BS">Bachelors</option>
                                                                        <option value="MS">Masters</option>
                                                                        <option value="Doctoral">Doctorate</option>
                                                                    </select>
                                                                    <br>
                                                                    <g:actionSubmit class='btn' name='update' value='Update'/>
                                                                </g:form>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    </g:form>
                                </g:each>
                            </table>
                            
                        </div>
                    </div>
                </div>
                <div class="col-md-2"> </div>
            </div>
        </div>
    </body>
</html>
