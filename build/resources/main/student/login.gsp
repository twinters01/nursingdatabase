<%@ page contentType="text/html;charset=UTF-8" %>
<html>
    <head>
        <title>Welcome to Student Registration</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
        <script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
        <link rel="stylesheet" type="text/css" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">

    </head>
    <body>
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-2"></div>
                <div class="col-md-8">
                    
                    <div class="row">
                        <div class="panel panel-default">
                            <div class="panel-heading">Login to view database</div>
                            <div class="panel-body">
                                <div class="col-md-4"></div>
                                <div class="col-md-4">
                                    <g:if test="${flash.error}">
                                        <div class="alert alert-danger">${flash.error}</div>
                                    </g:if>
                                    <g:if test="${flash.success}">
                                        <div class="alert alert-success">${flash.success}</div>
                                    </g:if>
                                    <g:if test="${flash.message}">
                                        <div class="alert alert-warning">${flash.message}</div>
                                    </g:if>
                                    <g:form controller="Student" action="list">
                                        <label for='password'>Password: </label>
                                        <div class="input-group">
                                            <g:textField class="form-control" name="password"/>
                                        </div>
                                        <g:actionSubmit class="btn btn-default" action='list' value='Login' name='login'/>
                                    </g:form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>                    
    </body>
</html>
